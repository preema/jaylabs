﻿using Puzzle.Service;
using Puzzle.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Puzzle.Controllers
{
    [RoutePrefix("Shape")]
    public class ShapeController : ApiController
    {
        private readonly IShapeService _shapeService;
        public ShapeController(IShapeService shapeService)
        {
            _shapeService = shapeService;
        }
        [Route("GenerateFromString/{text=text}")]
        [HttpGet]
        public IHttpActionResult GenerateShapeByStringParsing(string text)
        {
            if (string.IsNullOrEmpty(text))
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "invalid text"));

            var result = _shapeService.GenerateShapeFromString(text);

            if (!result.IsSuccess)
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, result.ErrorMessage));

            return Ok(result.Shape);
        }
    }
}
