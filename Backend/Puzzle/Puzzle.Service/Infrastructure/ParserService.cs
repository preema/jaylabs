﻿using System;
using System.Text.RegularExpressions;

namespace Puzzle.Service.Infrastructure
{
    public interface IParserService
    {
        string GetTextBetween(string source, string leftWord, string rightWord);
        string GetWordAfter(string source, string word);
    }
    public class ParserService : IParserService
    {
        public string GetTextBetween(string source, string leftWord, string rightWord)
        {
            return Regex.Match(source, String.Format(@"{0}\s(?<words>[\w\s]+)\s{1}", leftWord, rightWord),
                            RegexOptions.IgnoreCase).Groups["words"].Value;
        }
        public string GetWordAfter(string source , string word)
        {
            var pattern = @"\b" + Regex.Escape(word) + @"\s+(\w+)";
            return Regex.Match(source, pattern, RegexOptions.IgnoreCase).Groups[1].Value;
        }

    }
}
