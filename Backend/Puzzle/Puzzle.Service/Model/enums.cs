﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Puzzle.Service.Model
{
    public enum DimensionType
    {
        Radius,
        Side,
        Width,
        Height
    }
    public enum ShapeType
    {
        [Description("Isosceles Triangle")]
        IsoscelesTriangle = 1,
        [Description("Square")]
        Square = 2,
        [Description("Scalene Triangle")]
        ScaleneTriangle = 3,
        [Description("Parallelogram")]
        Parallelogram = 4,
        [Description("Equilateral Triangle")]
        EquilateralTriangle = 5,
        [Description("Pentagon")]
        Pentagon = 6,
        [Description("Rectangle")]
        Rectangle = 7,
        [Description("Hexagon")]
        Hexagon = 8,
        [Description("Heptagon")]
        Heptagon = 9,
        [Description("Octagon")]
        Octagon = 10,
        [Description("Circle")]
        Circle = 11,
        [Description("Oval")]
        Oval = 12,
        None = 0
    }
}
