﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Puzzle.Service.Model
{
    public class ShapeResult
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public Shape Shape { get; set; }


    }
}
