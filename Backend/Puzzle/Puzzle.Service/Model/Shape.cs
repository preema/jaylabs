﻿using System.Collections.Generic;

namespace Puzzle.Service.Model
{
    public class Shape
    {
        public string Type { get; set; }
        public IEnumerable<Dimension> Dimensions { get; set; }

        public string ValidateShape()
        {
            string message = string.Empty;
            foreach (var dimension in Dimensions)
            {
                if (dimension.Value <= 0)
                    message = message + dimension.Name + " is invalid .";
            }
            return message;
        }
    }

}
