﻿using Puzzle.Service.Extention;
using Puzzle.Service.Infrastructure;
using Puzzle.Service.Model;
using System.Collections.Generic;

namespace Puzzle.Service
{
    public interface IShapeService
    {
        ShapeResult GenerateShapeFromString(string text);

    }
    public class ShapeService : IShapeService
    {
        private readonly IParserService _parserService;
        public ShapeService(IParserService parserService)
        {
            _parserService = parserService;
        }
        public ShapeResult GenerateShapeFromString(string text)
        {
            var shape = GetShape(text);

            if (shape != ShapeType.None)
                return GenerateShape(shape, text);
            else
                return new ShapeResult() { IsSuccess = false, ErrorMessage = "Invalid Shape" };
        }

        private ShapeResult GenerateShape(ShapeType type, string text)
        {
            int height, width, side, radius;

            int.TryParse(_parserService.GetWordAfter(text, "radius of"), out radius);
            int.TryParse(_parserService.GetWordAfter(text, "height of"), out height);
            int.TryParse(_parserService.GetWordAfter(text, "width of"), out width);
            int.TryParse(_parserService.GetWordAfter(text, "side length of"), out side);
            
            var dimentions = new List<Dimension>();
            switch (type)
            {
                case ShapeType.Circle:
                     dimentions.Add(CreateDimention(DimensionType.Radius, radius));
                    break;
                case ShapeType.Oval:
                    dimentions.Add(CreateDimention(DimensionType.Width, width));
                    dimentions.Add(CreateDimention(DimensionType.Height, height));
                    break;
                case ShapeType.EquilateralTriangle:
                    dimentions.Add(CreateDimention(DimensionType.Side, side));
                   
                    break;
                case ShapeType.IsoscelesTriangle:
                    dimentions.Add(CreateDimention(DimensionType.Width, width));
                    dimentions.Add(CreateDimention(DimensionType.Height, height));
                    break;
                case ShapeType.ScaleneTriangle:
                    dimentions.Add(CreateDimention(DimensionType.Width, width));
                    dimentions.Add(CreateDimention(DimensionType.Height, height));
                    break;

                case ShapeType.Square:
                    dimentions.Add(CreateDimention(DimensionType.Side, side));
                    break;
                case ShapeType.Rectangle:
                    dimentions.Add(CreateDimention(DimensionType.Width, width));
                    dimentions.Add(CreateDimention(DimensionType.Height, height));
                    break;
                case ShapeType.Parallelogram:
                    dimentions.Add(CreateDimention(DimensionType.Width, width));
                    dimentions.Add(CreateDimention(DimensionType.Height, height));
                    break;
                case ShapeType.Pentagon:
                    dimentions.Add(CreateDimention(DimensionType.Side, side));
                  break;
                case ShapeType.Hexagon:
                    dimentions.Add(CreateDimention(DimensionType.Side, side));
                    break;
                case ShapeType.Heptagon:
                    dimentions.Add(CreateDimention(DimensionType.Side, side));
                    break;
                case ShapeType.Octagon:
                    dimentions.Add(CreateDimention(DimensionType.Side, side));
                    break;
                default:

                    break;
            }
            var shape = new Shape() { Type = type.ToString(), Dimensions = dimentions };

            var validationMessage = shape.ValidateShape();

            if (string.IsNullOrEmpty(validationMessage))
            {
                return new ShapeResult()
                {
                    ErrorMessage = "",
                    IsSuccess = true,
                    Shape = new Shape() { Type = type.ToString(), Dimensions = dimentions }
                };
            }
            else
            {
                return new ShapeResult()
                {
                    ErrorMessage = validationMessage,
                    IsSuccess = false,
                    Shape = null
                };
            }

        }

        private Dimension CreateDimention(DimensionType dimension, int value)
        {
            return new Dimension { Name = dimension.ToString().ToLower(), Value = value };
        }

        private ShapeType GetShape(string text)
        {

            var shape = _parserService.GetTextBetween(text, "Draw a", "with");

            if (string.IsNullOrEmpty(shape))
                shape = _parserService.GetTextBetween(text, "Draw an", "with");

            return EnumExtention.GetEnumValueFromDescription<ShapeType>(shape);
        }
    }
}
