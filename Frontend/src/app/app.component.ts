import { Component, OnInit } from '@angular/core';
import { ShapeService } from "./shape.service";
import { Shape } from './shape.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  shapeText : string = "";
  title = 'app';
  shape : Shape
  errorMessage : string;
  constructor(private _shapeService: ShapeService){} 

  ngOnInit(){ }

  onGenerateShapeClicked(){
    this.errorMessage = "";
    this._shapeService.generateShape(this.shapeText).subscribe((shape : Shape) => {
      this.shape = shape;
    },error => {
      var err = error.json();
      this.errorMessage = err.message
    })
  }
}
