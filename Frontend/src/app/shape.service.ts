import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions  } from '@angular/http';
import { Observable, Subject, throwError} from 'rxjs';
import { map } from 'rxjs/operators';
import { Shape } from './shape.model';

@Injectable()
export class ShapeService {
    private _serviceUrl = "http://jaylabpuzzle/";
    constructor(private _http: Http){ }

    generateShape(text: string = null): Observable<Shape> {
        let url = this._serviceUrl + "shape/GenerateFromString";
        if (text != null)
            url = url + "?text=" + text;

        return this._http.get(url)
            .pipe(map((response: Response) => {
             let shape =   <Shape>response.json();
             return shape;
        }));

    }
}