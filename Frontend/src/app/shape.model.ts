export interface Shape
{
  type: string;
  dimensions : Dimension[];
}

export interface Dimension
{
  name: string;
  value : string;
}