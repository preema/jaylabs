import { Component, OnInit, Input, OnChanges, AfterViewInit, ViewChild, ElementRef,Renderer } from '@angular/core';
import { Shape, Dimension } from '../shape.model';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'draw-shape',
  templateUrl: './drawshape.component.html',
  styleUrls: ['./drawshape.component.css']
})
export class DrawshapeComponent implements OnInit,OnChanges, AfterViewInit {
  @Input() shape : Shape;
  @ViewChild('canvas') canvasRef:ElementRef;
  private canvas: any;
  private canvasContext : any;
  constructor(private renderer: Renderer) { }
  
  ngOnInit() {
    
  }
  ngAfterViewInit() {
    this.canvas = this.canvasRef.nativeElement;
    this.canvasContext = this.canvas.getContext('2d');
    this.canvasContext.strokeStyle = "red";
  }
  ngOnChanges() {
   if(this.canvasContext)
    this.canvasContext.clearRect(0, 0, this.canvasContext.canvas.width, this.canvasContext.canvas.height);

    if(this.shape){
      if(this.shape.type == "Circle")
          this.drawCircle(this.shape.dimensions);
      else if(this.shape.type == "Oval")
          this.drawOval(this.shape.dimensions);
      else if(this.shape.type == "Square")
          this.drawSquare(this.shape.dimensions);
      else if(this.shape.type == "IsoscelesTriangle")
          this.drawTriangle(this.shape.dimensions, this.shape.type);
      else if(this.shape.type == "EquilateralTriangle")
          this.drawTriangle(this.shape.dimensions, this.shape.type);
      else if(this.shape.type == "Rectangle")      
          this.drawRectangle(this.shape.dimensions);
      else if(this.shape.type == "Pentagon")
          this.drawnthSideShape(this.shape.dimensions, 5);
      else if(this.shape.type == "Hexagon")
          this.drawnthSideShape(this.shape.dimensions, 6);
      else if(this.shape.type == "Heptagon")
         this.drawnthSideShape(this.shape.dimensions, 7);
      else if(this.shape.type == "Octagon")
         this.drawnthSideShape(this.shape.dimensions, 8);
   }
}

drawCircle(dimensions :Dimension[] ){
  let radius = dimensions.find(x => x.name == "radius").value;
  var canvas = this.canvasContext.canvas;
  var centerX = canvas.width / 2
  var centerY = canvas.height / 2;
  
  this.canvasContext.beginPath();
  this.canvasContext.arc(centerX, centerY, radius, 0, 2*Math.PI, false);
  this.canvasContext.stroke();
 }

 drawOval(dimensions :Dimension[]){
    var width  = dimensions.find(x => x.name == "width").value;
    let height = dimensions.find(x => x.name == "height").value ;
    var canvas = this.canvasContext.canvas;
    var centerX = canvas.width / 2
    var centerY = canvas.height / 2;
   
    this.canvasContext.ellipse(centerX, centerY, width, height, 0, 0, 2*Math.PI, false);
    this.canvasContext.stroke();
    
   }

 drawSquare(dimensions :Dimension[] ){
    let side : any = dimensions.find(x => x.name == "side").value
    this.canvasContext.beginPath();
    var x = this.canvasContext.canvas.width/2;
    var y = this.canvasContext.canvas.height / 2 - side / 2 ;
    this.canvasContext.rect(x, y,side, side);
    this.canvasContext.stroke();
 }

 drawTriangle(dimensions :Dimension[], type: string ){
     if(type == "IsoscelesTriangle" ){
       var width: any = dimensions.find(x => x.name == "width").value;
       var height :any = dimensions.find(x => x.name == "height").value ;
     }
     else if(type == "EquilateralTriangle" ){
        var width: any = dimensions.find(x => x.name == "side").value;
        var height :any = dimensions.find(x => x.name == "side").value ;
    }
    var canvas = this.canvasContext.canvas;
    var x = canvas.width/2;
    var y = canvas.height / 2 - width / 2 ;
    
    this.canvasContext.beginPath();
    this.canvasContext.moveTo(x, y);
    this.canvasContext.lineTo(x + width / 2, y + height);
    this.canvasContext.lineTo(x - width / 2, y + height);
    this.canvasContext.closePath();
    this.canvasContext.stroke();

 }

 drawRectangle(dimensions :Dimension[]){
    let width : any = dimensions.find(x => x.name == "width").value;
    let height = dimensions.find(x => x.name == "height").value ;
    var x = this.canvasContext.canvas.width/2;
    var y = this.canvasContext.canvas.height / 2 - width / 2 ;
    this.canvasContext.beginPath();
    this.canvasContext.rect(x, y,width,height);
    this.canvasContext.stroke();
 }

 drawnthSideShape(dimensions :Dimension[], numberOfSides : number){
    var size : any  = dimensions.find(x => x.name == "side").value;
    var canvas = this.canvasContext.canvas;
    var Xcenter = canvas.width/2;
    var Ycenter = canvas.height/2 - size / 2 ;
    var step  = 2 * Math.PI / numberOfSides;
    var shift = (Math.PI / 180.0) * -18;

    this.canvasContext.beginPath();
    for (var i = 0; i <= numberOfSides;i++) {
        var curStep = i * step + shift;
        this.canvasContext.lineTo (Xcenter + size * Math.cos(curStep), Ycenter + size * Math.sin(curStep));
    }

    this.canvasContext.lineWidth = 1;
    this.canvasContext.stroke();
   }

 
}
